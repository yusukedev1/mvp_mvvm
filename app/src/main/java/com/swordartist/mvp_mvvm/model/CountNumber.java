package com.swordartist.mvp_mvvm.model;

/**
 * Created by Yusuke on 2017-10-30.
 */

public class CountNumber {

    public CountNumber() {
        this.countNumber = 0;
    }

    public CountNumber(int countNumber) {
        this.countNumber = countNumber;
    }

    public int getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(int countNumber) {
        this.countNumber = countNumber;
    }

    int countNumber;

}
