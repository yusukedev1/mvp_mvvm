package com.swordartist.mvp_mvvm.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.swordartist.mvp_mvvm.R;
import com.swordartist.mvp_mvvm.model.CountNumber;
import com.swordartist.mvp_mvvm.presenter.ClickNumberPresenter;

public class ClickNumberActivity extends AppCompatActivity implements ClickNumberView {
    // Presenter
    ClickNumberPresenter mClickNumberPresenter;

    // UI references
    TextView mCountNumberTextView;
    Button mCountUpButton;
    Button mCountDownButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_number);

        // set UI references
        mCountNumberTextView = findViewById(R.id.countNumberTextView);
        mCountUpButton = findViewById(R.id.countUpButton);
        mCountDownButton = findViewById(R.id.countDownButton);

        // Register Action Listeners
        mCountUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCountUp(view);
            }
        });

        mCountDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCountDown(view);
            }
        });

        // create presenter
        mClickNumberPresenter = ClickNumberPresenter.newInstance(this);

        mClickNumberPresenter.onCreate();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mClickNumberPresenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mClickNumberPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mClickNumberPresenter.onDestroy();
    }

    @Override
    public void upDateCounter(int count) {
        mCountNumberTextView.setText(String.valueOf(count));
    }

    @Override
    public void resetCounter() {
        mCountNumberTextView.setText("0");
    }

    public void onClickCountUp(View v) {
        mClickNumberPresenter.onCountUp();
    }

    public void onClickCountDown(View v) {
        mClickNumberPresenter.onCountDown();
    }
}
