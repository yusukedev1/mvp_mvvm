package com.swordartist.mvp_mvvm.view;

/**
 * Created by Yusuke on 2017-10-30.
 */

public interface ClickNumberView {
    void upDateCounter(int count);

    void resetCounter();
}
