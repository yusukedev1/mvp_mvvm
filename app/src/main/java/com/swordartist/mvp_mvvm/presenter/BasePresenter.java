package com.swordartist.mvp_mvvm.presenter;

/**
 * Created by Yusuke on 2017-10-30.
 */

public interface BasePresenter {
    void onCreate();

    void onPause();

    void onResume();

    void onDestroy();
}
