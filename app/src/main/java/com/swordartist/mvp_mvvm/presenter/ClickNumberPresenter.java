package com.swordartist.mvp_mvvm.presenter;

import com.swordartist.mvp_mvvm.model.CountNumber;
import com.swordartist.mvp_mvvm.view.ClickNumberView;

/**
 * Created by Yusuke on 2017-10-30.
 */

public class ClickNumberPresenter implements BasePresenter {

    // References for view and model
    ClickNumberView mClickNumberView;
    CountNumber mCountNumber;

    static public ClickNumberPresenter newInstance(ClickNumberView clickNumberView) {
        ClickNumberPresenter clickNumberPresenter = new ClickNumberPresenter();
        clickNumberPresenter.mClickNumberView = clickNumberView;
        clickNumberPresenter.mCountNumber = new CountNumber();
        return clickNumberPresenter;
    }

    public void onCountUp() {
        // counting up the number in the model
        int countNumber = mCountNumber.getCountNumber();
        countNumber++;
        mCountNumber.setCountNumber(countNumber);

        // update UI
        mClickNumberView.upDateCounter(countNumber);
    }

    public void onCountDown() {
        // counting down the number in the model
        int countNumber = mCountNumber.getCountNumber();
        countNumber--;
        mCountNumber.setCountNumber(countNumber);

        // update UI
        mClickNumberView.upDateCounter(countNumber);
    }

    @Override
    public void onCreate() {
        mClickNumberView.resetCounter();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }
}
